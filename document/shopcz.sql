/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.26 : Database - shopcz
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`shopcz` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `shopcz`;

/*Table structure for table `cz_address` */

DROP TABLE IF EXISTS `cz_address`;

CREATE TABLE `cz_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '地址编号',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '地址所属用户ID',
  `consignee` varchar(60) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `province` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '省份，保存是ID',
  `city` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '市',
  `district` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '区',
  `street` varchar(100) NOT NULL DEFAULT '' COMMENT '街道地址',
  `zipcode` varchar(10) NOT NULL DEFAULT '' COMMENT '邮政编码',
  `telephone` varchar(20) NOT NULL DEFAULT '' COMMENT '电话',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '移动电话',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cz_address` */

/*Table structure for table `cz_admin` */

DROP TABLE IF EXISTS `cz_admin`;

CREATE TABLE `cz_admin` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员编号',
  `admin_name` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员名称',
  `password` char(50) NOT NULL DEFAULT '' COMMENT '管理员密码',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '管理员邮箱',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `cz_admin` */

insert  into `cz_admin`(`id`,`admin_name`,`password`,`email`,`add_time`) values (1,'admin','40bd001563085fc35165329ea1ff5c5ecbdbbeef','admin@itcast.cn','2022-03-29 21:36:02');

/*Table structure for table `cz_attribute` */

DROP TABLE IF EXISTS `cz_attribute`;

CREATE TABLE `cz_attribute` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品属性ID',
  `attr_name` varchar(50) NOT NULL DEFAULT '' COMMENT '商品属性名称',
  `type_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '商品属性所属类型ID',
  `attr_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '属性是否可选 0 为唯一，1为单选，2为多选',
  `attr_input_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '属性录入方式 0为手工录入，1为从列表中选择，2为文本域',
  `attr_value` text COMMENT '属性的值',
  `sort_order` tinyint(4) NOT NULL DEFAULT '50' COMMENT '属性排序依据',
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `cz_attribute` */

insert  into `cz_attribute`(`id`,`attr_name`,`type_id`,`attr_type`,`attr_input_type`,`attr_value`,`sort_order`) values (1,'出版社',0,0,0,'',50);

/*Table structure for table `cz_brand` */

DROP TABLE IF EXISTS `cz_brand`;

CREATE TABLE `cz_brand` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品品牌ID',
  `brand_name` varchar(30) NOT NULL DEFAULT '' COMMENT '商品品牌名称',
  `brand_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '商品品牌描述',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '商品品牌网址',
  `logo` varchar(250) NOT NULL DEFAULT '' COMMENT '品牌logo',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '50' COMMENT '商品品牌排序依据',
  `is_show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示，默认显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `cz_brand` */

insert  into `cz_brand`(`id`,`brand_name`,`brand_desc`,`url`,`logo`,`sort_order`,`is_show`) values (1,'诺基亚1','坚强的手机品牌，但是已没落','http://WWW.NOKIA.COM','./Public/Uploads/2017-04-10/58eaeb45c1e60.png',1,1),(2,'李宁1','国际运动品牌','http://www.liling.com','./Public/Uploads/2017-04-10/58eaeb71d03b5.gif',2,1),(3,'安踏1','永不止步','http://www.anta.com','./Public/Uploads/2017-04-10/58eaeb954f99b.gif',3,1),(4,'联想','世界第一个人电脑制造商','http://www.lenovo.com','./Public/Uploads/2017-04-10/58eaebc5b9611.png',4,1),(5,'耐克','世界知名运动品牌','http://www.nike.com','./Public/Uploads/2017-04-10/58eaebf91da34.gif',5,1),(6,'NN11111222','NNNNNNNN11121111','http://www.NN.com','./Public/Uploads/2017-04-17/58f425a9e706c.gif',6,1),(7,'huawei','sdfsdfsdfsdf','www.huawei.com','',50,1),(9,'周大福','周大福周大福周大福周大福美丽人生','https://www.ctfmall.com/','/uploads/20220505/92a80aa829422abbd4d720af1281b039.jpg',50,1),(10,'周生生','周生生周生生周生生周生生周生生周生生','https://www.zssmall.com/','/uploads/20220505/0c319defda3e001e3059ccd5de9f99a3.jpg',50,1);

/*Table structure for table `cz_cart` */

DROP TABLE IF EXISTS `cz_cart`;

CREATE TABLE `cz_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '购物车ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `goods_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品ID',
  `goods_name` varchar(100) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_img` varchar(250) NOT NULL DEFAULT '' COMMENT '商品图片',
  `goods_attr` varchar(255) NOT NULL DEFAULT '' COMMENT '商品属性',
  `goods_number` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `market_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '市场价格',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '成交价格',
  `subtotal` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '小计',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `cz_cart` */

insert  into `cz_cart`(`id`,`user_id`,`goods_id`,`goods_name`,`goods_img`,`goods_attr`,`goods_number`,`market_price`,`goods_price`,`subtotal`) values (3,2,19,'兰兰精品大号女装XXXXXL','./Uploads/2017-06-01/592f7707656e5.jpg','',1,'420.00','168.00','168.00'),(7,2,16,'若如初见2017新款欧美潮','./Uploads/2017-06-01/592f764bd41f7.jpg','',1,'469.00','169.00','169.00');

/*Table structure for table `cz_category` */

DROP TABLE IF EXISTS `cz_category`;

CREATE TABLE `cz_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品类别ID',
  `cat_name` varchar(30) NOT NULL DEFAULT '' COMMENT '商品类别名称',
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '商品类别父ID',
  `cat_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '商品类别描述',
  `sort_order` tinyint(4) NOT NULL DEFAULT '50' COMMENT '排序依据',
  `unit` varchar(15) NOT NULL DEFAULT '' COMMENT '单位',
  `is_show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示，默认显示',
  PRIMARY KEY (`id`),
  KEY `pid` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `cz_category` */

insert  into `cz_category`(`id`,`cat_name`,`parent_id`,`cat_desc`,`sort_order`,`unit`,`is_show`) values (1,'男女服装',0,'',1,'件',1),(2,'鞋包配饰',0,'',2,'件',1),(3,'美容美妆',0,'',3,'件',1),(4,'家用电器',0,'',3,'',1),(5,'数码',0,'',4,'',1),(6,'家居',0,'',5,'',1),(7,'图书',0,'',50,'',1),(8,'美食',0,'',50,'',1),(9,'健康',0,'',50,'',0),(10,'体育用品',0,'',50,'',1),(11,'202移动2',0,'202移动2202移动2202移动2202移动2202移动2',21,'件',0);

/*Table structure for table `cz_galary` */

DROP TABLE IF EXISTS `cz_galary`;

CREATE TABLE `cz_galary` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '图片编号',
  `goods_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品ID',
  `img_url` varchar(50) NOT NULL DEFAULT '' COMMENT '图片URL',
  `thumb_url` varchar(50) NOT NULL DEFAULT '' COMMENT '缩略图URL',
  `img_desc` varchar(50) NOT NULL DEFAULT '' COMMENT '图片描述',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cz_galary` */

/*Table structure for table `cz_goods` */

DROP TABLE IF EXISTS `cz_goods`;

CREATE TABLE `cz_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `goods_sn` varchar(30) NOT NULL DEFAULT '' COMMENT '商品货号',
  `goods_name` varchar(100) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_brief` varchar(255) NOT NULL DEFAULT '' COMMENT '商品简单描述',
  `goods_desc` text COMMENT '商品详情',
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '商品所属类别ID',
  `brand_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '商品所属品牌ID',
  `market_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '市场价',
  `shop_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '本店价格',
  `promote_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '促销价格',
  `promote_start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '促销起始时间',
  `promote_end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '促销截止时间',
  `goods_img` varchar(250) NOT NULL DEFAULT '' COMMENT '商品图片',
  `goods_thumb` varchar(250) NOT NULL DEFAULT '' COMMENT '商品缩略图',
  `goods_number` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '商品库存',
  `click_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击次数',
  `type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '商品类型ID',
  `is_promote` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否促销，默认为0不促销',
  `is_best` tinyint(3) unsigned zerofill NOT NULL DEFAULT '000' COMMENT '是否精品,默认为0',
  `is_new` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否新品，默认为0',
  `is_hot` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否热卖,默认为0',
  `is_onsale` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否上架,默认为1',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `cat_id` (`cat_id`),
  KEY `brand_id` (`brand_id`),
  KEY `type_id` (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `cz_goods` */

insert  into `cz_goods`(`id`,`goods_sn`,`goods_name`,`goods_brief`,`goods_desc`,`cat_id`,`brand_id`,`market_price`,`shop_price`,`promote_price`,`promote_start_time`,`promote_end_time`,`goods_img`,`goods_thumb`,`goods_number`,`click_count`,`type_id`,`is_promote`,`is_best`,`is_new`,`is_hot`,`is_onsale`,`add_time`) values (22,'Daisygirl01','Daisygirl 2014 夏天 定制两','',NULL,0,1,'380.00','465.00','2750.00',0,0,'./uploads/2017-06-02/59310897468a7.jpg','',0,0,0,0,000,0,0,0,'2022-03-02 21:45:00'),(8,'LALABOBOS1','两件八折LALABOBO女装明','',NULL,1,0,'512.00','495.00','480.00',2009,2014,'','',0,0,0,0,000,0,0,1,'2022-03-02 21:45:00'),(9,'XXZ发AZ01','熙熙韩国正品2016夏季新','',NULL,1,0,'968.00','546.00','480.00',2009,2014,'./uploads/2017-06-01/592f736ef1dd3.jpg','',0,0,0,0,000,0,0,0,'2022-03-02 21:45:00'),(10,'MMZ01','玛玛绨2017春秋新款韩版','',NULL,1,0,'724.00','299.00','290.00',2009,2014,'./uploads/2017-06-01/592f73f2b98c1.jpg','',0,0,0,1,000,0,0,1,'2022-03-02 21:45:00'),(11,'cc01','咫尺2017春装新款中长款','',NULL,1,0,'670.00','268.00','260.00',2009,2014,'./uploads/2017-06-01/592f747e933db.jpg','',0,0,0,0,000,1,1,1,'2022-03-02 21:45:00'),(12,'pk01','PUNKRAVE朋克女装性感透','',NULL,1,0,'576.00','121.00','110.00',2009,2014,'./uploads/2017-06-01/592f74e1b8842.jpg','',0,0,0,0,000,0,0,1,'2022-03-02 21:45:00'),(13,'cp001','YIYG 潮牌ifashion欧洲站女','',NULL,2,0,'528.00','89.00','80.00',2009,2014,'./uploads/2017-06-01/592f752cb75da.jpg','',0,0,0,1,001,0,0,1,'2022-03-02 21:45:00'),(14,'zr001','藏肉神器 SUN 2017春新款','',NULL,1,0,'499.00','159.00','150.00',2009,2014,'./uploads/2017-06-01/592f7576cc54a.jpg','',0,0,0,1,000,1,0,1,'2022-03-02 21:45:00'),(15,'dz001','Dasiygirl 定制显瘦不规则设','',NULL,1,0,'499.00','249.00','240.00',2009,2014,'./uploads/2017-06-01/592f760bc0cba.jpg','',0,0,0,0,000,0,0,0,'2022-03-02 21:45:00'),(16,'rr001','若如初见2017新款欧美潮','',NULL,1,0,'469.00','169.00','160.00',2009,2014,'./uploads/2017-06-01/592f764bd41f7.jpg','',0,0,0,1,000,1,0,1,'2022-03-02 21:45:00'),(17,'cz001','COCOBELLA2017春装新款','',NULL,1,0,'498.00','229.00','220.00',2009,2014,'./uploads/2017-06-01/592f769e8b506.jpg','',0,0,0,0,001,0,0,1,'2022-03-02 21:45:00'),(18,'bz001','【本裁】牛仔水洗印花做旧','',NULL,1,0,'488.00','341.00','320.00',2009,2014,'./uploads/2017-06-01/592f76d920d92.jpg','',0,0,0,0,000,1,0,0,'2022-03-02 21:45:00'),(19,'ll001','兰兰精品大号女装XXXXXL','',NULL,1,0,'420.00','168.00','160.00',2009,2014,'./uploads/2017-06-01/592f7707656e5.jpg','',0,0,0,1,001,0,1,0,'2022-03-02 21:45:00'),(20,'sf001','【释缝】蕾丝连衣裙2017','',NULL,1,0,'399.00','399.00','300.00',2009,2014,'./uploads/2017-06-01/592f773c473e8.jpg','',0,0,0,0,001,1,0,0,'2022-03-02 21:45:00'),(21,'hh001','韩火火 2017春款时尚竖条','',NULL,1,0,'199.00','398.00','190.00',2009,2014,'./uploads/2017-06-01/592f776c84358.jpg','',0,0,0,0,000,0,1,1,'2022-03-02 21:45:00'),(23,'Highlines01','Highlines 纯色V领连体裤 黑','',NULL,1,0,'465.00','399.00','390.00',2009,2014,'./uploads/2017-06-02/593107f43d62a.jpg','',0,0,0,0,001,0,0,0,'2022-03-02 21:45:00'),(26,'10039029845783','小米Note11','',NULL,0,0,'1468.00','1450.00','14400.00',0,0,'./uploads/20220505/a3048a1fcd305f75789042841135d413.jpg','',0,0,0,0,000,0,1,1,'2022-05-05 10:42:17'),(27,'100031406014','小米121','',NULL,0,0,'3758.00','3688.00','3508.00',0,0,'./uploads/20220505/d30b11c968848a14ada1e642c4007652.jpg','',0,0,0,0,000,0,1,1,'2022-05-05 10:50:25');

/*Table structure for table `cz_goods_attr` */

DROP TABLE IF EXISTS `cz_goods_attr`;

CREATE TABLE `cz_goods_attr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `goods_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品ID',
  `attr_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '属性ID',
  `attr_value` varchar(255) NOT NULL DEFAULT '' COMMENT '属性值',
  `attr_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '属性价格',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `attr_id` (`attr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cz_goods_attr` */

/*Table structure for table `cz_goods_type` */

DROP TABLE IF EXISTS `cz_goods_type`;

CREATE TABLE `cz_goods_type` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品类型ID',
  `type_name` varchar(50) NOT NULL DEFAULT '' COMMENT '商品类型名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cz_goods_type` */

/*Table structure for table `cz_order` */

DROP TABLE IF EXISTS `cz_order`;

CREATE TABLE `cz_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `order_sn` varchar(30) NOT NULL DEFAULT '' COMMENT '订单号',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收货地址id',
  `order_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '订单状态 1 待付款 2 待发货 3 已发货 4 已完成',
  `postscripts` varchar(255) NOT NULL DEFAULT '' COMMENT '订单附言',
  `shipping_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '送货方式ID',
  `pay_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '支付方式ID',
  `goods_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品总金额',
  `order_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单总金额',
  `order_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `address_id` (`address_id`),
  KEY `pay_id` (`pay_id`),
  KEY `shipping_id` (`shipping_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `cz_order` */

insert  into `cz_order`(`id`,`order_sn`,`user_id`,`address_id`,`order_status`,`postscripts`,`shipping_id`,`pay_id`,`goods_amount`,`order_amount`,`order_time`) values (22,'20220602591O9R',1,0,1,'',0,0,'0.00','5138.00','2022-06-02 10:40:59'),(20,'20220602286M39',1,0,1,'',0,0,'0.00','1317.00','2022-06-02 10:36:28');

/*Table structure for table `cz_order_goods` */

DROP TABLE IF EXISTS `cz_order_goods`;

CREATE TABLE `cz_order_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '订单ID',
  `order_sn` varchar(30) DEFAULT '' COMMENT '订单号',
  `goods_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品ID',
  `goods_name` varchar(100) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_img` varchar(250) NOT NULL DEFAULT '' COMMENT '商品图片',
  `shop_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '成交价格',
  `goods_number` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '购买数量',
  `goods_attr` varchar(255) NOT NULL DEFAULT '' COMMENT '商品属性',
  `subtotal` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品小计',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `cz_order_goods` */

insert  into `cz_order_goods`(`id`,`order_id`,`order_sn`,`goods_id`,`goods_name`,`goods_img`,`shop_price`,`goods_price`,`goods_number`,`goods_attr`,`subtotal`) values (18,20,'20220602286M39',12,'PUNKRAVE朋克女装性感透','./Uploads/2017-06-01/592f74e1b8842.jpg','121.00','576.00',1,'','121.00'),(17,20,'20220602286M39',21,'韩火火 2017春款时尚竖条','./Uploads/2017-06-01/592f776c84358.jpg','398.00','199.00',1,'','398.00'),(16,20,'20220602286M39',23,'Highlines 纯色V领连体裤 黑','./uploads/2017-06-02/593107f43d62a.jpg','399.00','465.00',2,'','798.00'),(19,22,'20220602591O9R',26,'小米Note11','./uploads/20220505/a3048a1fcd305f75789042841135d413.jpg','1450.00','1468.00',1,'','1450.00'),(20,22,'20220602591O9R',27,'小米121','./uploads/20220505/d30b11c968848a14ada1e642c4007652.jpg','3688.00','3758.00',1,'','3688.00');

/*Table structure for table `cz_payment` */

DROP TABLE IF EXISTS `cz_payment`;

CREATE TABLE `cz_payment` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT '支付方式ID',
  `pay_name` varchar(30) NOT NULL DEFAULT '' COMMENT '支付方式名称',
  `pay_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '支付方式描述',
  `enabled` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否启用，默认启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cz_payment` */

/*Table structure for table `cz_region` */

DROP TABLE IF EXISTS `cz_region`;

CREATE TABLE `cz_region` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '地区ID',
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `region_name` varchar(30) NOT NULL DEFAULT '' COMMENT '地区名称',
  `region_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '地区类型 1 省份 2 市 3 区(县)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cz_region` */

/*Table structure for table `cz_shipping` */

DROP TABLE IF EXISTS `cz_shipping`;

CREATE TABLE `cz_shipping` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `shipping_name` varchar(30) NOT NULL DEFAULT '' COMMENT '送货方式名称',
  `shipping_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '送货方式描述',
  `shipping_fee` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '送货费用',
  `enabled` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否启用，默认启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `cz_shipping` */

/*Table structure for table `cz_user` */

DROP TABLE IF EXISTS `cz_user`;

CREATE TABLE `cz_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `password` char(50) NOT NULL DEFAULT '' COMMENT '用户密码,md5加密',
  `reg_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户注册时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `cz_user` */

insert  into `cz_user`(`id`,`user_name`,`email`,`password`,`reg_time`) values (1,'cz1','cz1@163.com','40bd001563085fc35165329ea1ff5c5ecbdbbeef','2022-03-09 19:20:45'),(2,'cz2','cz2@126.com','40bd001563085fc35165329ea1ff5c5ecbdbbeef','2022-03-09 19:20:45'),(3,'cz3','cz3@gmail.com','40bd001563085fc35165329ea1ff5c5ecbdbbeef','2022-03-09 19:20:45'),(8,'cz4','sdf!@123.com','40bd001563085fc35165329ea1ff5c5ecbdbbeef','2022-03-09 19:20:45');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
