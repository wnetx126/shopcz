<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\index\controller;
use think\Controller;
use think\Request;
use think\Db;
/**
 * Description of Login
 *
 * @author Administrator
 */
class Login extends Controller{
    //put your code here
    public function index(){
//        return "Login index";
        return view("/login/login");
    }
    
    //用户登录处理
    public function check(Request $request){
        //第一步：接收前端传递的参数并处理
        var_dump($request->param());
        echo "<br>";
//        $params=$request->except("code"); //except 排除某个传递参数
        $params=$request->param();
        var_dump($params);
        echo "<br>";
        $data['user_name']=$params['user'];
        $data['password']=$params['pwd'];
        $data['code']=$params['code'];
        //验证码 
        //要使用tp自带的验证码需要通过composer 安装tp验证码的包  composer require topthink/think-captcha=2.0.*
        if(!captcha_check($data['code'])){
            //验证失败
            $this->error("验证码错误！", "/index/login/index");
        }
        var_dump($data);
        echo "<br>";
     
        //第二步：操作表并返回结果
        $result=Db::name('user')->where('user_name', $data['user_name'])
                ->where('password',sha1($data['password']))
                ->select();
        echo "<br>";
        var_dump($result);
        echo "<br>";
        //第三步:处理返回结果
        //empty(数组)用来判断数组的内容是否是空值
        if(!empty($result)){
            $user=$result[0];
            $cz_uname=$user['user_name'];
            $cz_uid=$user['id'];
            cookie("cz_uname", $cz_uname,time()+60*60*24*7); //7天内有效 写用户名到cookie中
            cookie("cz_uid", $cz_uid,time()+60*60*24*7);     //7天内有效 写用户ID到cookie中
            $this->success("恭喜你登录成功！", "/index/home/index");
        }else{
            $this->error("用户名或密码输入错误，请重新登录!", "/index/login/index");
        }
          
    }
}
