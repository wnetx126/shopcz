<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\index\controller;
use app\index\model\Goods;
use think\Controller;
use think\Request;
/**
 *商品的详情页
 *
 * @author Administrator
 */
class Detail extends Controller{
    //put your code here
    
    public function index(Request $request){
        $id= input('id');
        $data=Goods::where("id",$id)->find(); //方法OK
//        $data=Goods::get(23);//方法OK
//        var_dump($data);
//        echo "<br>1111111111111111111</br>";
//        return "123";
        return view("/good/goods",["data"=>$data]);
    }
}
