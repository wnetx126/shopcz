<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\index\controller;
use think\Controller;
use think\Request;
use app\index\model\Order as OrderModel;
use app\index\model\OrderGoods as OrderGoodsModel;
use app\index\model\Cart as CartModel;
  /**
    * 订单管理
    * 功能：提交订单
    * 描述：在购物车中提交订单并跳转到订单列表页面 
    *      订单表cz_order（订单编码，用户ID,订单状态，商品总价）         订单状态默认0 ，1待支付
    *      订单详情表cz_order_goods（订单编码，用户ID，商品ID，商品名称，商品图片，商品数量，商品单价，商品小计）
    * 思路：1.生成订单表数据；
    *      2.生成订单详情表数据
    *      3.清空购物车中该用户已下单的商品数据
    *      4.跳转到订单列表页
    * 
   */
class Order extends Controller{
     //显示商品列表
    public function index(Request $request){
        $uid= cookie("cz_uid"); //获取登录用户ID
        $orders=OrderModel::where("user_id",$uid)->order("id","desc")->select();
        var_dump($orders);
//        echo "<br>111111111111111111</br>";
        foreach ($orders as $value) {
            $orderGoodsTmp=OrderGoodsModel::where("order_id",$value["id"])->select();
            $value["order_goods"]=$orderGoodsTmp;//订单对象中添加一个临时属性order_goods ，用来存储对于的订单详情表的数据
        }
        var_dump($orders);
//        echo "<br>2222222222222</br>";
//        exit();
        return view("/order/order",["data"=>$orders]);
    }
     /**
     * 显示添加页面/处理添加数据过程
     * @param Request $request
     * @return string
     */
    public function add(Request $request){
        $uid= cookie("cz_uid"); //获取登录用户ID
//        echo "<br>1111111111<br>";
//        exit();
        if($request->isPost()){//post提交
            //1.接收页面传递的参数，并处理
//            1.生成订单表数据；
            $data['order_amount']=input('order_amount');
            $data['order_sn']=$this->generateNum();
            $data['user_id']=$uid;
            $data['order_status']=1;//待支付状态
            //2.操作数据表模型并返回结果
            //当前返回结果为订单对象数组数据
            $order=OrderModel::create($data);
            var_dump($order);
//            exit();
            //通过查询购物车数据组合成订单详情表数 2.生成订单详情表数据
            $condition['user_id']=$uid;
            $cartModel=new CartModel();
            $carts=$cartModel->where($condition)->select();
            $orderGoodsData=array();
            foreach ($carts as $key => $value){
                $orderGoodsTmp['order_id']=$order['id'];
                $orderGoodsTmp['order_sn']=$order["order_sn"];
                $orderGoodsTmp['goods_id']=$value['goods_id'];
                $orderGoodsTmp['goods_name']=$value['goods_name'];
                $orderGoodsTmp['goods_img']=$value['goods_img'];
                $orderGoodsTmp['shop_price']=$value['goods_price'];
                $orderGoodsTmp['goods_price']=$value['market_price'];
                $orderGoodsTmp['goods_number']=$value['goods_number'];
                $orderGoodsTmp['subtotal']=$value['subtotal'];
                $orderGoodsData[]=$orderGoodsTmp;
            }
            var_dump($orderGoodsData);
            //根据已组合的订单详情表数据插入订单详情表中
            $orderGoodsModel=new OrderGoodsModel();
            $flag=$orderGoodsModel->saveAll($orderGoodsData,false);
//             3.处理返回结果
            if($flag){
//                 3.清空购物车中该用户已下单的商品数据
               $cartFlag=$cartModel->where($condition)->delete();
               $this->success("下单成功","/index/order/index");
            }else{
               $this->redirect("下单失败", "/index/cart/index");
            }
//          
//            if($flag){
//                return $this->success("订单添加成功!", "/index/cart/index");
//            }else{
//                return $this->error("订单添加失败!", "/index/cart/index");
//            }
        }else{//get
            
            return view(); 
        }
        return "Order add";
    }
    /**
     * 显示修改页面/处理修改数据过程
     * @param Request $request
     * @return string
     */
    public function edit(Request $request){
         if($request->isPost()){//post提交
            //1.接收页面传递的参数，并处理
            
            //2.操作数据表模型并返回结果
            
            //3.处理返回结果
        }else{//get
            
            return view(); 
        }
        return "Order edit";
    }
    /**
     * 删除一条数据
     * @param Request $request
     * @return string
     */
    public function del(Request $request){
        
        return "Order del";
    }
    
//    /**
//     * 生成订单编号
//     * @return string
//     */
//    function generateNum(){
//        $yCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
//        $order_number = $yCode[intval(date('Y')) - 2012] .date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 6), 1))), 0,4);
//        return $order_number;
//    }
     /**
    * 生成随机字符串实例代码（字母+数字）
    * 生成一个随机字符串时，总是先创建一个字符池，然后用一个循环和mt_rand()或rand()生成php随机数，从字符池中随机选取字符，最后拼凑出需要的长度
    * @param type $length
    * @return string
    * 返回值 如果是6个长度返回: MII3J6
    */        
    function randomkeys($length) { 
    $pattern ="1234567890ABCDEFGHIJKLOMNOPQRSTUVWXYZ";
    $key="";
    for($i=0;$i<$length;$i++){ 
     $key .= $pattern{mt_rand(0,36)}; //生成php随机数 
    } 
    return $key; 
    } 
    /**
    * 生成订单号
    * @return string
    * 返回值类似: 20201125081130MII3J6
    */
    function generateNum(){
       $order_number =  date('Ymds').$this->randomkeys(4);
       return $order_number;
    }
}
