<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\index\controller;
use app\index\model\Goods;
use think\Db;
/**
 * Description of Home
 *
 * @author Administrator
 */
class Home {
    //put your code here
    public function index(){
//        return "home index";
//      查询特别推荐商品的数据
        $result=Db::name('goods')->where("is_best",1)->limit(4)->order("id desc")->select(); //条件查询where("is_best",1) 排序order("id desc") 限制查询个数limit(4)
//      热门商品
        $hotData=Db::name('goods')->where("is_hot",1)->limit(4)->order("goods_name desc")->select();
//      新品上架
        $newData=Goods::where("is_new",1)->limit(4)->order("id desc")->select();
//        var_dump($newData);
//        echo "<br>11111111111111<br>";
//        var_dump($newData[0]);
//        echo "<br>22222222222222<br>";
//        return "123";
        //助手函数view(模板,模板变量) 模板变量可以不写
//        return view("/home/index");
//        return view("/home/index",["title"=>"shopcz首页测试"]);
        return view("/home/index",['data'=>$result,'hotData'=>$hotData,"newData"=>$newData]);
    }
}
