<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\index\controller;
use think\Controller;
use think\Request;
use app\index\model\Goods as GoodsModel;
/**
 * Description of Listing
 *
 * @author Administrator
 */
class Listing extends Controller{
    //put your code here
    public function index(Request $request){
        $result=GoodsModel::order("id","desc")->paginate(8);
//        $result=GoodsModel::order("id","desc")->all();
        return view("list/list",["data"=>$result]);
    }
}
