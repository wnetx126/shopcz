<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\index\controller;
use think\Controller;
use think\facade\Cookie;
/**
 * 用户登录注销
 *
 * @author Administrator
 */
class Logout extends Controller{
    //put your code here
    public function index(){
        //清空cookie信息 使用的Cookie的门面模式或外观模式的方法
        if(Cookie::has('cz_uid')){
            Cookie::delete("cz_uname");//清除用户名 cookie
            Cookie::delete("cz_uid"); //清除用户id cookie
        }
        //页面重定向redirect
        return $this->redirect("/index/home/index");
    }
}
