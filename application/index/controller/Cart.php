<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\index\controller;
use think\Controller;
use think\Request;
use app\index\model\Cart as CartModel;
/**
 * Description of Cart
 *
 * @author Administrator
 */
class Cart extends Controller{

    //显示商品列表
    public function index(Request $request){
        $uid= cookie("cz_uid"); //获取登录用户ID
        //查询当前用户下的购物车商品信息
        $result=CartModel::where("user_id",$uid)->order('id', 'desc')->select();
        return view("cart/flow2",["data"=>$result]);
//        return "Cart index";
    }
     /**
     *
     * 功能描述：添加商品到购物车
     * 关键问题：如果某个用户的购物车已经有添加的商品，怎么处理？
     * 解题思路：如果某个用户的购物车中没有已经添加的商品则直接添加到购物车中，如果某个用户的购物车中有已添加的商品则更新商品的数量
     * 显示添加页面/处理添加数据过程
     * @param Request $request
     * @return string
     */
    public function add(Request $request){
        $uid= cookie("cz_uid"); //获取登录用户ID
        echo"<br>111111111<br>";
        if($request->isPost()){//post提交
            //1.接收页面传递的参数，并处理
            $data=$request->only(["goods_id","goods_name","goods_number","goods_img","market_price","goods_price"]);
            $data['user_id']=$uid;
            $data["subtotal"]=$data['goods_price']*$data["goods_number"];
            var_dump($data);
            echo"<br>22222222<br>";
            //2.操作数据表模型并返回结果
            //查询将要添加购物车中的商品数据是否已经存在购物车中 如果是 只需要更新商品数量，否 插入商品的数据
            $where['user_id']=$data['user_id'];
            $where['goods_id']=$data['goods_id'];
            $cart=CartModel::where($where)->find();
            $result=null;
            if($cart){
                //需要更新商品的数量及小计
                $data['goods_number']=$cart['goods_number']+$data['goods_number'];//更新购物车商品的数量
                $data["subtotal"]=$data['goods_price']*$data["goods_number"];     //更新购物车中小计的值
                $cartModel=new CartModel();
                $result=$cartModel->save($data, $where);
            }else{
                //直接插入一条购物车数据
                $result=CartModel::create($data); //插入一条数据
            }
            //3.处理返回结果
            if($result){
                return $this->success("购物车添加成功!", "/index/cart/index");
            }else{
                return $this->error("购物车添加失败!", "/index/listing/index");
            }
        }else{//get
            
            return view(); 
        }
        return "Cart add";
    }
    /**
     * 显示修改页面/处理修改数据过程
     * @param Request $request
     * @return string
     */
    public function edit(Request $request){
         if($request->isPost()){//post提交
            //1.接收页面传递的参数，并处理
            
            //2.操作数据表模型并返回结果
            
            //3.处理返回结果
        }else{//get
            
            return view(); 
        }
        return "Cart edit";
    }
    /**
     * 删除一条数据
     * @param Request $request
     * @return string
     */
    public function del(Request $request){
         $id=input("id");//input 为helper助手函数 可以得到某个参数的值 不分get post
        $result=CartModel::destroy($id);//删除某条或多条数据
        if($result){
            return $this->success("购物车商品删除成功!", "/index/cart/index");
        }else{
            return $this->error("购物车商品删除失败!", "/index/cart/index");
        }
//        return "Cart del";
    }
}
