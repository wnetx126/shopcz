<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\admin\controller;
use think\Controller;
use think\Request;
use app\admin\model\Goods as GoodsModel;
/**
 * 商品管理
 * @author Administrator
 */
class Goods extends Controller{
    //显示商品列表
    public function index(Request $request){
        $result=GoodsModel::order("id","desc")->paginate(2);
        return view("goods/goods_list",["data"=>$result]);
//        return "Goods index";
    }
     /**
     * 显示添加页面/处理添加数据过程
     * @param Request $request
     * @return string
     */
    public function add(Request $request){
        if($request->isPost()){//post提交
            //1.接收页面传递的参数，并处理
            $data=$request->only(["goods_name","goods_sn","shop_price","market_price","promote_price"]);
             //文件上传开始start
            if($_FILES['goods_img']['tmp_name']){ //判断是否上传过临时文件副本到web服务器
                $file = request()->file('goods_img');
                if($file){
                    // 移动到框架应用根目录/uploads/ 目录下
                    $info = $file->move( './uploads'); //把上传过临时文件副本转成正式文件并转移uploads/ 目录下
                    $data["goods_img"] =".". substr(str_ireplace('\\', '/', $info->getPathName()), 1);//拼凑数据表需要胡文件路经信息
                }
            }
            //2.操作数据表模型并返回结果
            $result=GoodsModel::create($data); //插入一条数据
            //3.处理返回结果
            if($result){
                return $this->success("商品添加成功!", "/admin/goods/index");
            }else{
                return $this->error("商品添加失败!", "/admin/goods/index");
            }
        }else{//get
            return view("goods/goods_add");
        }
//        return "Goods add";
    }
    /**
     * 显示修改页面/处理修改数据过程
     * @param Request $request
     * @return string
     */
    public function edit(Request $request){
          if($request->isPost()){//post提交 处理前端POST数据
            //1.接收页面传递的参数，并处理
            $data=$request->only(["goods_name","goods_sn","shop_price","market_price","promote_price"]);
            //文件上传开始start
            if($_FILES['goods_img']['tmp_name']){ //判断是否上传过临时文件副本到web服务器
                $file = request()->file('goods_img');
                if($file){
                    // 移动到框架应用根目录/uploads/ 目录下
                    $info = $file->move( './uploads'); //把上传过临时文件副本转成正式文件并转移uploads/ 目录下
                    $data["goods_img"] =".". substr(str_ireplace('\\', '/', $info->getPathName()), 1);//拼凑数据表需要胡文件路经信息
                }
            }
            //2.操作数据表模型并返回结果
            $goodsModel=new GoodsModel();
            $result=$goodsModel->save($data, ["id"=>input("id")]);
            if($result){
                return $this->success("商品修改成功!", "/admin/goods/index");
            }else{
                return $this->error("商品修改失败!", "/admin/goods/index");
            }
            //3.处理返回结果
        }else{//get 显示修改页面
            $id=input("id");
            $result=GoodsModel::get($id);
            return view("goods/goods_edit",["data"=>$result]);
        }   
//        return "Goods edit";
    }
    /**
     * 删除一条数据
     * @param Request $request
     * @return string
     */
    public function del(Request $request){
        $id=input("id");//input 为helper助手函数 可以得到某个参数的值 不分get post
        $result=GoodsModel::destroy($id);//删除某条或多条数据
        if($result){
            return $this->success("商品删除成功!", "/admin/goods/index");
        }else{
            return $this->error("商品删除失败!", "/admin/goods/index");
        }
    }
    
    
}
