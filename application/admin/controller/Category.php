<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\admin\controller;
use think\Controller;
use think\Request;
use app\admin\model\Category as CategoryModel;
/**
 * 分类管理
 * @author Administrator
 */
class Category  extends Controller{
    
    //显示分类列表
    public function index(Request $request){
        $result=CategoryModel::order("id","desc")->paginate(100); 
        var_dump($result);
        return view("cate/cat_list",["data"=>$result]);
//        return "category index";
    }
    
     /**
     * 显示添加页面/处理添加数据过程
     * @param Request $request
     * @return string
     */
    public function add(Request $request){
        if($request->isPost()){
            //1.接收页面传递的参数，并处理
//            var_dump($request->param());
//            echo "<br>11111<br>";
            $data=$request->only(["cat_name","unit","cat_desc","sort_order","is_show"]);
//            var_dump($data);
//            echo "<br>2222222<br>";
            //2.操作数据表模型并返回结果
            $result=CategoryModel::create($data); //插入一条数据
//            var_dump($result);
            //3.处理返回结果
            if($result){
                return $this->success("分类数据添加成功!", "/admin/category/index");
            }else{
                return $this->error("分类数据添加失败!", "/admin/category/index");
            }
        }else{
          return view("cate/cat_add");  
        }
//        return "category add";
    }
    /**
     * 显示修改页面/处理修改数据过程
     * @param Request $request
     * @return string
     */
    public function edit(Request $request){
        if($request->isPost()){
              //1.接收页面传递的参数，并处理
            $data=$request->only(["cat_name","unit","cat_desc","sort_order","is_show"]);
//            var_dump($data);
//            echo "<br>2222222<br>";
            //2.操作数据表模型并返回结果
            $categoryModel=new CategoryModel();
            $result=$categoryModel->save($data, ['id' => input("id")]); 
//            var_dump($result);
            //3.处理返回结果
            if($result){
                return $this->success("分类数据修改成功!", "/admin/category/index");
            }else{
                return $this->error("分类数据修改失败!", "/admin/category/index");
            }
        }else{
            $id= input("id");
           $result=CategoryModel::get($id); 
           return view("cate/cat_edit",["data"=>$result]); 
        }
//        return "category edit";
    }
    /**
     * 删除一条数据
     * @param Request $request
     * @return string
     */
    public function del(Request $request){
        $id=input("id");//input 为helper助手函数 可以得到某个参数的值 不分get post
        $result=CategoryModel::destroy($id);//删除某条或多条数据
        if($result){
            return $this->success("分类数据删除成功!", "/admin/brand/index");
        }else{
            return $this->error("分类数据删除失败!", "/admin/brand/del/id/"+$id);
        }
//        return "category del";
    }   
}
