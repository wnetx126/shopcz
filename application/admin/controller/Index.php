<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\admin\controller;
use think\Controller;
use think\Request;
/**
 * admin模块下的首页
 * @author Administrator
 */
class Index extends Controller{
    //put your code here
    public function index(Request $request){
        
        return view("/index/index");
//        return "Admin Index index";
    }
    
    public function main(Request $request){
        
        return view("/index/main");
    }
    
    public function top(Request $request){
        
        return view("/index/top");
    }
    
    public function menu(Request $request){
        
        return view("/index/menu");
    }
    
    public function drag(Request $request){
        
        return view("/index/drag");
    }
    
}
