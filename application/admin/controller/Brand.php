<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\admin\controller;
use think\Controller;
use think\Request;
use app\admin\model\Brand as BrandModel;
/**
 * admin模块 品牌控制器
 *
 * @author Administrator
 */
class Brand  extends Controller{
    /**
     * 品牌列表
     * @param Request $request
     * @return string
     */
    public function index(Request $request) {
//        $result=BrandModel::all();//查询所有的数据
        $result=BrandModel::order("id","desc")->paginate(2); //每页两条数据
        var_dump($result);
        echo "<br>22222222<br>";
        $this->assign('data',$result);    
        return $this->fetch("/brand/brand_list");// 渲染模板输出
//        return view("/brand/brand_list",["data"=>$result]); //OK 等同 $this->assign(key,value)   $this->fetch(模板)
    }
    /**
     * 显示添加页面/处理添加数据过程
     * @param Request $request
     * @return string
     * @date 2020-05-05
     * @author wnetx126
     * @modify wnetx126
     */
    public function add(Request $request) {
        if($request->isPost()){//post提交
//            添加品牌的数据
             //1.接收页面传递的参数，并处理
            $param=$request->param();
            var_dump($param);
            echo "<br>11111111111<br>";
            $data['brand_name']=$param['brand_name'];
            $data['url']=$param['url'];
            $data['brand_desc']=$param['brand_desc'];
            $data['sort_order']=$param['sort_order'];
            $data['is_show']=$param['is_show'];
            //文件上传开始start
            if($_FILES['logo']['tmp_name']){ //判断是否上传过临时文件副本到web服务器
                $file = request()->file('logo');
                if($file){
                    // 移动到框架应用根目录/uploads/ 目录下
                    $info = $file->move( './uploads'); //把上传过临时文件副本转成正式文件并转移uploads/ 目录下
                    $data["logo"]=$pathname = substr(str_ireplace('\\', '/', $info->getPathName()), 1);//拼凑数据表需要胡文件路经信息
                }
            }
            //文件上传结束end
            //2.操作数据表并返回结果
            $result=BrandModel::create($data);
            var_dump($result);
            echo "<br>22222222<br>";
            //3.处理返回结果
            return $this->success("品牌数据添加成功!", "/admin/brand/index");
        }else{//get 提交
            //显示品牌页面
            return view("/brand/brand_add");
        }
//        return "brand add";
    }
    /**
     * 显示修改页面/处理修改数据过程
     * @param Request $request
     * @return string
     */
    public function edit(Request $request) {
        if($request->isPost()){//post提交 处理修改数据过程
            //修改品牌的数据
             //1.接收页面传递的参数，并处理
            $param=$request->param();
            var_dump($param);
            echo "<br>11111111111<br>";
            $data['brand_name']=$param['brand_name'];
            $data['url']=$param['url'];
            $data['brand_desc']=$param['brand_desc'];
            $data['sort_order']=$param['sort_order'];
            $data['is_show']=$param['is_show'];
             //文件上传开始start
            if($_FILES['logo']['tmp_name']){ //判断是否上传过临时文件副本到web服务器
                $file = request()->file('logo');
                if($file){
                    // 移动到框架应用根目录/uploads/ 目录下
                    $info = $file->move( './uploads');
                    $data["logo"]=$pathname = substr(str_ireplace('\\', '/', $info->getPathName()), 1);
                }
            }
            //文件上传结束end
            //2.操作数据表并返回结果
            $brandModel=new BrandModel();
            $result=$brandModel->save($data, ['id' => $param['id']]); 
            echo "<br>111111111111<br>";
            var_dump($result);
            echo "<br>22222222222<br>";
            //3.处理返回结果
            return $this->success("品牌数据编辑成功!", "/admin/brand/index");
        }else{//显示修改页面
            $id=input("id"); //input 为helper助手函数 可以得到某个参数的值 不分get post
            $result=BrandModel::get($id);
//            var_dump($result);
            return view("/brand/brand_edit",["data"=>$result]);
        }
//        return "brand edit";
    }
    /**
     * 删除一条数据
     * @param Request $request
     * @return string
     */
    public function del(Request $request) {
        $id=input("id");//input 为helper助手函数 可以得到某个参数的值 不分get post
        $result=BrandModel::destroy($id);//删除某条或多条数据
        if($result){
            return $this->success("品牌数据删除成功!", "/admin/brand/index");
        }else{
            return $this->error("品牌数据删除失败!", "/admin/brand/del/id/"+$id);
        }
//        return "brand del";
    }
    
}
