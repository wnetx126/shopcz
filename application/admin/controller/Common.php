<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\admin\controller;
use think\Controller;
use think\Request;
/**
 * 通用模板
 * @author Administrator
 */
class Common extends Controller{
    //put your code here
     //显示商品列表
    public function index(Request $request){
        
        return "Goods index";
    }
     /**
     * 显示添加页面/处理添加数据过程
     * @param Request $request
     * @return string
     */
    public function add(Request $request){
        if($request->isPost()){//post提交
            //1.接收页面传递的参数，并处理
            
            //2.操作数据表模型并返回结果
            
            //3.处理返回结果
        }else{//get
            
            return view(); 
        }
        return "Goods add";
    }
    /**
     * 显示修改页面/处理修改数据过程
     * @param Request $request
     * @return string
     */
    public function edit(Request $request){
         if($request->isPost()){//post提交
            //1.接收页面传递的参数，并处理
            
            //2.操作数据表模型并返回结果
            
            //3.处理返回结果
        }else{//get
            
            return view(); 
        }
        return "Goods edit";
    }
    /**
     * 删除一条数据
     * @param Request $request
     * @return string
     */
    public function del(Request $request){
        
        return "Goods del";
    }
}
